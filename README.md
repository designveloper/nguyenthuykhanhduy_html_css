# HTML

## Introduction

### HTML resources

* https://www.w3.org/
* https://www.whatwg.org/
* https://www.webplatform.org/
* https://developer.mozilla.org/

### Content models

* Metadata content is content that sets up the presentation or behavior of the rest of the content (link, meta, script, ...)

* Flow content: Most elements that are used in the body of documents

* Sectioning content is content that defines the scope of headings and footers (section, nav, ...)

* Heading content defines the header of a section (h1, h2, ...)

* Phrasing content is the text of the document (a, b, br, ...)

* Embedded content is content that imports another resource into the document (audio, canvas, iframe, ...)

* Interactive content is content that is specifically intended for user interaction (button, input, ...)

## Formatting page content

### Formatting page content with HTML

```
<pre>
Text in a pre element
is displayed in a fixed-width
font, and it preserves
both      spaces and
line breaks
</pre>

Use the <pre> element when displaying text with unusual formatting, or some sort of computer code.
```

### Formatting paragraphs

* collapsing margin: In CSS, the adjoining margins of two or more boxes (which might or might not be siblings) can combine to form a single margin. Margins that combine this way are said to collapse, and the resulting combined margin is called a collapsed margin.

## Structuring Content

### Nav
* defines a set of navigation links
```
<nav>
  <a href="/html/">HTML</a> |
  <a href="/css/">CSS</a> |
  <a href="/js/">JavaScript</a> |
  <a href="/jquery/">jQuery</a>
</nav>
```

### Article
* specifies independent, self-contained content (Forum post, Blog post)

### Section
* defines sections in a document, such as chapters, headers, footers, or any other sections of the document

### Aside
* defines some content aside from the content it is placed in.

### Div
* defines a division or a section in an HTML document

## Creating Links

### Link to page within your site
```
<a href="<link>"></>
```

### Link to external page
```
<a href="<link>" target="_blank"></a>
```

* default target is _self

### Link to downloadable resources
```
<a href="<link>" download></a>
```

### Link to page regions
```
<a href="#id"></a>
```

## Creating lists

### Unordered lists
```
<ul>
    <li></li>
    <li></li>
</ul>
```

### Ordered lists
```
<ol>
    <li></li>
</ol>
```
* type: "1", "A", "a",...
* start (By default, an ordered list will start counting from 1. If you want to start counting from a specified number, you can use the start attribute)

### Definition lists
```
<dl>
  <dt></dt>
  <dd></dd>
</dl>
```

# CSS

## Introduction

### Inline, internal and external CSS

* external CSS: seperate CSS file, referenced within \<head\>
```
<link rel="stylesheet" href="">
```
* inline: use style attribute, override external CSS
* internal CSS: override external CSS if it's added after
```
use <style></style>
```

### Naming conventions

* use lowercase
* use underscore/dash to seperate words

## CSS Core

### Type, class and id selectors

* Type: match HTML by using element name
```
h1 { 

}
```
* Class

    + Can be use multiple times per page

    + selector starting with a period

* Multiple classes

    + Separate mutiple classes with a space

    + Combine classes, with no space

* ID

    + IDs can only be used once per page
    
    + selector starting with # 

### Pseudo-Class Selectors

* Use multiple selectors, separated by a space, to match the descendant elements

* Group mutiple selector
```
h1, h2 { }
```

* Pseudo-Class Selectors: specify a state of the element and using a colon
```
a:hover { }
```

### Selectors best practices

* Type Selector: Used to select all or most instances of an element
* Class Selector: Used fo more specific styles that can also be applied to diffrent elements, one ore more times per page
* ID Selector: use them for unique or global styles that are not repeated
* Descendant Selectors: based on its parent or ancestor element. Avoid going more than three levels deep
* Combining Selectors: Combine selectors to apply the same style to different elements, in one declaration block

### Cascading, inheritance and specificity

* style atrribute > id > class, attribule, psuedo class > element
* element: 1; class, attribule, psuedo class: 10; id: 100; style atrribute: 1000


## Typography

### Web fonts
* Internal font source

    + Downloaded font files

    + Included in project and declared using @font-face
    
    ```
    @font-face {
        font-family: 'Museo Sans';
        src: url(museo-sans.ttf)
    }
    body {
        font-family: 'Museo Sans', Arial, sans-serif;
    }
    ```
* External font source

    + No need to download font file

    + Link directly to CSS and font files hosted online

### font-size property

* px

    + absolute value

    + greate for accuracy

* em

    + relative unit

    + 1em = inherited font-size
    
    + if no font-size is declared, 1em = default = 16px

* rem

    + relative unit but only to the root element

### font-style and font-weight

* font-weight thickness or boldness of typefaces
* font-style used to add or remove an italic style

### color, line-hegiht and text properties

* text-decoration used to remove or add an underline above, below, or through the text
* text-transform specific the letter casing the values are capitial, uppercase, lowercase
* text-align: align text left, right or center

## Layouts

### Block vs Inline

* Block

    + Height = content, Width = 100% of container

    + Elements start on a new line

    +  h1...h6, div, p

* Inline

    + Height and width = content

    + Elements align left, in a line
    
    + a, span, strong

### Box Model

* Width and height: sets specific size for the content box
* Padding: space inside of the element
* Margin: space outside of the element
* Border: displays between the padding and margin